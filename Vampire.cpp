/*********************************************************************
** Program name: Vampire.cpp
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Vampire class implementation file.
 * Child of Character.
*********************************************************************/

#include "Vampire.h"


// Default constructor
Vampire::Vampire()
		:Character(1,18, "Vampire", 0, 0, 0, 0, 0)
{
	// For seeding dies
	srand(time(0));
}

// Optional constructor
Vampire::Vampire(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
		int actualDamageInflictedIn, int damageAttackedIn, int livesIn)
		:Character(armorIn, strengthPointsIn, nameIn, attackPointsIn, defensePointsIn, actualDamageInflictedIn,
				damageAttackedIn, livesIn)
{
	// For seeding dies
	srand(time(0));
}

// Attack
// Rolling 1 Twelve sided dice
int Vampire::attack()
{
	int rollOne = 0;
	rollOne = rand() % 12 + 1;
	attackPoints = rollOne;
	cout << "Attack by " << name << ": " << endl;
	cout << "Roll one " << rollOne << endl;
	cout << "Total attack points " << attackPoints << endl;

	printEndFormat();

	return attackPoints;



}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void Vampire::defense()
** Defense
// 1 Six Sided dice
// With charm
// Charm: Vampires can charm an opponent into not attacking.
// For a given attack there is a 50% chance that their opponent
// does not actually attack them.
******************************************************************************/
int Vampire::defense(int damageAttackedIn)
{

	int rollOne = 0;
	rollOne = rand() % 6 + 1;
	defensePoints = rollOne;
	cout << "Defense by " << name << endl;
	cout << "Roll one " << rollOne << endl;

	strengthPoints = defenseCommonCode();

	return strengthPoints;
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void Vampire::charm()
** Charm: Vampires can charm an opponent into not attacking.
// For a given attack there is a 50% chance that their opponent
// does not actually attack them.
// [To Be Implemented]
******************************************************************************/
void Vampire::charm()
{
	cout << "Vampire charm applied. " << endl;
	cout << "There is a 50% chance that the opponent will not attack." << endl;
}

// Destructor
Vampire::~Vampire()
{

}

