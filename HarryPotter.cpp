/*********************************************************************
** Program name: HarryPotter.cpp
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: HarryPotter class implementation file.
 * Child of Character.
*********************************************************************/

#include "HarryPotter.h"

// Default constructor
HarryPotter::HarryPotter()
		:Character(0,10, "Harry Potter", 0, 0, 0, 0, 0)
{
	// For seeding dies
	srand(time(0));
}

// Optional constructor
HarryPotter::HarryPotter(int lives, int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
		int actualDamageInflictedIn, int damageAttackedIn, int livesIn)
		:Character(armorIn, strengthPointsIn, nameIn, attackPointsIn, defensePointsIn, actualDamageInflictedIn,
				damageAttackedIn, livesIn)
{

	this->lives = lives;

	// For seeding dies
	srand(time(0));
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void HarryPotter::attack()
** // Attack
// Rolling 2 - 6 sided dies
******************************************************************************/

int HarryPotter::attack()
{
	int rollOne = 0;
	int rollTwo = 0;
	rollOne = rand() % 6 + 1;
	rollTwo = rand() % 6 + 1;
	attackPoints = rollOne + rollTwo;

	cout << "Attack by " << name << ": " << endl;
	cout << "Roll one " << rollOne << endl;
	cout << "Roll two " << rollTwo << endl;
	cout << "Total attack points " << attackPoints << endl;

	printEndFormat();

	return attackPoints;
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void HarryPotter::defense()
** Defense
** 2 Six Sided dice
******************************************************************************/
int HarryPotter::defense(int damageAttackedIn)
{
	int rollOne = 0;
	int rollTwo = 0;
	rollOne = rand() % 6 + 1;
	rollTwo = rand() % 6 + 1;

	defensePoints = rollOne + rollTwo;

	cout << "Defense by " << name << endl;
	cout << "Roll one " << rollOne << endl;
	cout << "Roll two " << rollTwo << endl;

	cout << "Defense points " << defensePoints << endl;

	actualDamageInflicted = damageAttackedIn - defensePoints - armor;

	cout << "Damage attacked in " << damageAttackedIn << endl;
	cout << "Armor " << armor << endl;
	cout << "Actual Damage Inflicted " << actualDamageInflicted << endl;
	cout << "Strength points at start " << strengthPoints << endl;

	// Apply the damage to the defender's strength points
	strengthPoints = strengthPoints - actualDamageInflicted;

	if(strengthPoints <= 0)
	{
		// Case 1 - first life - will be reborn
		if(lives == 0)
		{
			cout << "Strength points currently " << strengthPoints << endl;
			cout << "Harry Potter dies. But, this is his first time." << endl;
			cout << "Strength points being increased to 20." << endl;
			strengthPoints = 20;
			increaseLives();
			cout << "Strength points currently " << strengthPoints << endl;
		}
		else // Case 2 - second life - will die
		{
			cout << "Harry Potter is dead. " << endl;
			cout << "Strength points currently " << strengthPoints << endl;
		}

	}
	else
	{
		cout << "Strength points currently " << strengthPoints << endl;
	}

	printEndFormat();

	return strengthPoints;
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void HarryPotter::hogwarts()
** Hogwarts: If Harry Potter's strength reaches 0 or below, he immediately
 * recovers and his total strength becomes 20. If he were to die again, then he’s dead.
 * [To Be Implemented]
******************************************************************************/
void HarryPotter::hogwarts()
{
	cout << "HarryPotter hogwarts: " << endl;
	cout << "Hogwarts: If Harry Potter's strength reaches 0 or below, he "
		 "immediately recovers and his total strength becomes 20. "
   			"If he were to die again, then he’s dead." << endl;
}

// Destructor
HarryPotter::~HarryPotter()
{

}
