/*********************************************************************
** Program name: Menu.cpp
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Menu Class implementation file
*********************************************************************/
#include "Menu.h"


template<typename T>
void Menu::getChoice(T &choice, string input)
{
	cout << input;
	cin >> setw(1) >> choice; // Just get one option

	// Error checking
	while(!cin.good() || choice < 1 || choice > 5)
	{
		// Report problem
		cout << "ERROR: Try again..." << endl;

		// Clear the stream

		// Clear buffer
		cin.clear();
		// Clear newline
		cin.ignore(INT_MAX, '\n');

		// Get input again
		cout << input;
		cin >> setw(1) >> choice; // Just get one option

	}

	// Clear the stream

	// Clear buffer
	cin.clear();
	// Clear newline
	cin.ignore(INT_MAX, '\n');
}



void Menu::displayCharacters() {
	// If this is an int the whole function will work as int
	int choice1 = -1;
	int choice2 = -1;

	string input1 = "Please pick the first character from the following characters: \n";
	input1 += "1. Vampire\n";
	input1 += "2. Barbarian\n";
	input1 += "3. Blue Men\n";
	input1 += "4. Medusa\n";
	input1 += "5. Harry Potter\n";

	getChoice(choice1, input1);

	string input2 = "Please pick the second character from the following characters: \n";
	input2 += "1. Vampire\n";
	input2 += "2. Barbarian\n";
	input2 += "3. Blue Men\n";
	input2 += "4. Medusa\n";
	input2 += "5. Harry Potter\n";

	getChoice(choice2, input2);


	// Anything less than 1 will quit the program
	while (choice1 > 0 && choice2 > 0) // Menu
	{
		// Vampire start
		// Case 1: Vampire vs Vampire
		if(choice1 == 1 && choice2 == 1)
		{
			cout << "Vampire vs Vampire" << endl;
			firstChoice = 1;
			secondChoice = 1;
			break;
		}

		// Case 2: Vampire vs Barbarian
		if(choice1 == 1 && choice2 == 2)
		{
			cout << "Vampire vs Barbarian" << endl;
			firstChoice = 1;
			secondChoice = 2;
			break;
		}

		// Case 3: Vampire vs BlueMen
		if(choice1 == 1 && choice2 == 3)
		{
			cout << "Vampire vs BlueMen" << endl;
			firstChoice = 1;
			secondChoice = 3;
			break;
		}

		// Case 4: Vampire vs Medusa
		if(choice1 == 1 && choice2 == 4)
		{
			cout << "Vampire vs Medusa" << endl;
			firstChoice = 1;
			secondChoice = 4;
			break;
		}

		// Case 5: Vampire vs Harry Potter
		if(choice1 == 1 && choice2 == 5)
		{
			cout << "Vampire vs Harry Potter" << endl;
			firstChoice = 1;
			secondChoice = 5;
			break;
		}

		// Vampire end
		// Barbarian start

		// Case 1: Barbarian vs Vampire
		if(choice1 == 2 && choice2 == 1)
		{
			cout << "Barbarian vs Vampire" << endl;
			firstChoice = 1;
			secondChoice = 6;
			break;
		}

		// Case 2: Barbarian vs Barbarian
		if(choice1 == 2 && choice2 == 2)
		{
			cout << "Barbarian vs Barbarian" << endl;
			firstChoice = 2;
			secondChoice = 2;
			break;
		}

		// Case 3: Barbarian vs BlueMen
		if(choice1 == 2 && choice2 == 3)
		{
			cout << "Barbarian vs BlueMen" << endl;
			firstChoice = 2;
			secondChoice = 3;
			break;
		}

		// Case 4: Barbarian vs Medusa
		if(choice1 == 2 && choice2 == 4)
		{
			cout << "Barbarian vs Medusa" << endl;
			firstChoice = 2;
			secondChoice = 4;
			break;
		}

		// Case 5: Barbarian vs Harry Potter
		if(choice1 == 2 && choice2 == 5)
		{
			cout << "Barbarian vs Harry Potter" << endl;
			firstChoice = 2;
			secondChoice = 5;
			break;
		}

		// Barbarian end
		// Bluemen start

		// Case 1: BlueMen vs Vampire
		if(choice1 == 3 && choice2 == 1)
		{
			cout << "BlueMen vs Vampire" << endl;
			firstChoice = 3;
			secondChoice = 1;
			break;
		}

		// Case 2: BlueMen vs Barbarian
		if(choice1 == 3 && choice2 == 2)
		{
			cout << "BlueMen vs Barbarian" << endl;
			firstChoice = 3;
			secondChoice = 2;
			break;
		}

		// Case 3: BlueMen vs BlueMen
		if(choice1 == 3 && choice2 == 3)
		{
			cout << "BlueMen vs BlueMen" << endl;
			firstChoice = 3;
			secondChoice = 3;
			break;
		}

		// Case 4: BlueMen vs Medusa
		if(choice1 == 3 && choice2 == 4)
		{
			cout << "BlueMen vs Medusa" << endl;
			firstChoice = 3;
			secondChoice = 4;
			break;
		}

		// Case 5: BlueMen vs Harry Potter
		if(choice1 == 3 && choice2 == 5)
		{
			cout << "BlueMen vs Harry Potter" << endl;
			firstChoice = 3;
			secondChoice = 5;
			break;
		}

		// BlueMen end
		// Medusa start

		// Case 1: Medusa vs Vampire
		if(choice1 == 4 && choice2 == 1)
		{
			cout << "Medusa vs Vampire" << endl;
			firstChoice = 4;
			secondChoice = 1;
			break;
		}

		// Case 2: Medusa vs Barbarian
		if(choice1 == 4 && choice2 == 2)
		{
			cout << "Medusa vs Barbarian" << endl;
			firstChoice = 4;
			secondChoice = 2;
			break;
		}

		// Case 3: Medusa vs BlueMen
		if(choice1 == 4 && choice2 == 3)
		{
			cout << "Medusa vs BlueMen" << endl;
			firstChoice = 4;
			secondChoice = 3;
			break;
		}

		// Case 4: Medusa vs Medusa
		if(choice1 == 4 && choice2 == 4)
		{
			cout << "Medusa vs Medusa" << endl;
			firstChoice = 4;
			secondChoice = 4;
			break;
		}

		// Case 5: Medusa vs Harry Potter
		if(choice1 == 4 && choice2 == 5)
		{
			cout << "Medusa vs Harry Potter" << endl;
			firstChoice = 4;
			secondChoice = 5;
			break;
		}

		// Medusa end
		// Harry Potter start

		// Case 1: HarryPotter vs Vampire
		if(choice1 == 5 && choice2 == 1)
		{
			cout << "HarryPotter vs Vampire" << endl;
			firstChoice = 5;
			secondChoice = 1;
			break;
		}

		// Case 2: HarryPotter vs Barbarian
		if(choice1 == 5 && choice2 == 2)
		{
			cout << "HarryPotter vs Barbarian" << endl;
			firstChoice = 5;
			secondChoice = 2;
			break;
		}

		// Case 3: HarryPotter vs BlueMen
		if(choice1 == 5 && choice2 == 3)
		{
			cout << "HarryPotter vs BlueMen" << endl;
			firstChoice = 5;
			secondChoice = 3;
			break;
		}

		// Case 4: HarryPotter vs Medusa
		if(choice1 == 5 && choice2 == 4)
		{
			cout << "HarryPotter vs Medusa" << endl;
			firstChoice = 5;
			secondChoice = 4;
			break;
		}

		// Case 5: HarryPotter vs Harry Potter
		if(choice1 == 5 && choice2 == 5)
		{
			cout << "HarryPotter vs Harry Potter" << endl;
			firstChoice = 5;
			secondChoice = 5;
			break;
		}

		// Harry Potter end


		/*getChoice(choice1, input1);
		getChoice(choice2, input2);*/

	}

}


// Choice validation for play again
template <typename T>
void Menu::playAgainGetChoice(T& choice, string input)
{
	cout << input;
	cin >> setw(1) >> choice; // Just get one option

	// Error checking
	while(!cin.good() || choice < 1 || choice > 2)
	{
		// Report problem
		cout << "ERROR: Try again..." << endl;

		// Clear the stream

		// Clear buffer
		cin.clear();
		// Clear newline
		cin.ignore(INT_MAX, '\n');

		// Get input again
		cout << input;
		cin >> setw(1) >> choice; // Just get one option

	}

	// Clear the stream

	// Clear buffer
	cin.clear();
	// Clear newline
	cin.ignore(INT_MAX, '\n');
}


int Menu::playAgain()
{
	// If this is an int the whole function will work as int
	int choice = -1;

	string input = "Please enter 1 to play again and 2 to exit the game.";

	playAgainGetChoice(choice, input);

	// Anything less than 1 will quit the program
	while (choice > 0) // Menu
	{
		switch (choice)
		{
			case 1:
				cout << "Play again..." << "\n";
				initialSetup();
				break;

			case 2:
				cout << "Quit" << "\n";
				return 0;
				break;

			default:
				cout << "No such option" << "\n";
				break;
		}

		playAgainGetChoice(choice, input);
	}

	return 0;
}

int Menu::getFirstChoice() const {
	return firstChoice;
}

int Menu::getSecondChoice() const {
	return secondChoice;
}

// Initial setup from main (refactor)
void Menu::initialSetup()
{
	displayCharacters();
	cout << firstChoice << endl;
	cout << secondChoice << endl;

	switch(firstChoice)
	{
		case 1:
		{
			charPtr1 = new Vampire;
			break;
		}
		case 2:
		{
			charPtr1 = new Barbarian;
			break;
		}
		case 3:
		{
			charPtr1 = new BlueMen;
			break;
		}
		case 4:
		{
			charPtr1 = new Medusa;
			break;
		}
		case 5:
		{
			charPtr1 = new HarryPotter;
			break;
		}
		default:
		{
			cout << "This should not happen" << endl;
			break;
		}

	}

	switch(secondChoice)
	{
		case 1:
		{
			charPtr2 = new Vampire;
			break;
		}
		case 2:
		{
			charPtr2 = new Barbarian;
			break;
		}
		case 3:
		{
			charPtr2 = new BlueMen;
			break;
		}
		case 4:
		{
			charPtr2 = new Medusa;
			break;
		}
		case 5:
		{
			charPtr2 = new HarryPotter;
			break;
		}
		default:
		{
			cout << "This should not happen" << endl;
			break;
		}

	}

	play(charPtr1, charPtr2);


	delete charPtr1;
	delete charPtr2;
}


// Constructor
Menu::Menu()
{
	charPtr1 = nullptr;
	charPtr2 = nullptr;
}

int Menu::play(Character *charOne, Character *charTwo)
{
	// For seeding the 50% chance
	srand(time(0));
	int chance = 0;

	int numRound = 1;

	// Round code start

	cout << "\n\n\n" << endl;
	cout << "Round # " << numRound << endl;

	cout << "Character 1 attack" << endl;
	cout << "Character 1 type " << charOne->getName();
	cout << "\n" << endl;

	// Char 1 attacks Char 2 and damageAttackedIn is set in charTwo
	if(charTwo->getName() == "Vampire") // charTwo Vampire
	{
		// If Char2 is vampire attack only 50% of the time
		// This will only happen 50% of the time
		// This code produces either a 2 or a 1 randomly
		chance = rand() % 2 + 1;

		if(chance == 1) // attack on getting 1 as a the random number
		{
			charOne->attack();
			// Check to see if attacker is Medusa and rolls a 12
			if(charOne->getName() == "Medusa" && charOne->getAttackPoints() == 12)
			{
				cout << "Character 1 is Medusa and has rolled a 12. Character 2 is turned into stone with the"
						" Medusa glare. Character 1 wins." << endl;


				// charTwo is Harry Potter and he is dying for the first time
				if(charTwo->getName() == "Harry Potter" && charTwo->getLives() == 0)
				{
					// If it is Harry Potter and it is his first time dying,
					// game is not over.
					cout << "But, Character 2 is Harry Potter and it is his first life." << endl;
					cout << "He uses Hogwarts to increase his strength to 20." << endl;

					charTwo->setStrengthPoints(20);
					charTwo->increaseLives();
					cout << "Harry Potter's current strength points are " << charTwo->getStrengthPoints() << endl;
				}
				else // No one else can escape Medusa's glare so for everyone else game over
				{
					return 1;
				}


			}
			charTwo->setDamageAttackedIn(charOne->getAttackPoints());

			// Using that damageAttackedIn from above
			// charTwo uses the defense function to set the strength points within charTwo
			charTwo->defense(charTwo->getDamageAttackedIn());

		}
		else // don't attack on 2
		{
			cout << "Character 2 is a vampire and charm was used so no attack by Character 1 or defense"
					" by character 2." << endl;
		}
	}
	else // charTwo not vampire
	{
		// If char2 is not a vampire attack all the time
		charOne->attack();
		// Check to see if attacker is Medusa and rolls a 12
		if(charOne->getName() == "Medusa" && charOne->getAttackPoints() == 12)
		{
			cout << "Character 1 is Medusa and has rolled a 12. Character 2 is turned into stone with the"
					" Medusa glare. Character 1 wins." << endl;

			// charTwo is Harry Potter and he is dying for the first time
			if(charTwo->getName() == "Harry Potter" && charTwo->getLives() == 0)
			{
				// If it is Harry Potter and it is his first time dying,
				// game is not over.
				cout << "But, Character 2 is Harry Potter and it is his first life." << endl;
				cout << "He uses Hogwarts to increase his strength to 20." << endl;

				charTwo->setStrengthPoints(20);
				charTwo->increaseLives();
				cout << "Harry Potter's current strength points are " << charTwo->getStrengthPoints() << endl;
			}
			else // No one else can escape Medusa's glare so for everyone else game over
			{
				return 1;
			}

		}
		charTwo->setDamageAttackedIn(charOne->getAttackPoints());

		// Using that damageAttackedIn from above
		// charTwo uses the defense function to set the strength points within charTwo
		charTwo->defense(charTwo->getDamageAttackedIn());
	}


	cout << "Character 2 attack" << endl;
	cout << "Character 2 type " << charTwo->getName();
	cout << "\n" << endl;

	// If charOne is a vampire
	// charTwo will only attack charOne 50% of the time
	if(charOne->getName() == "Vampire")
	{
		chance = rand() % 2 + 1;

		if(chance == 1) // attack
		{
			// Char 2 attacks char 1
			// Set the damageAttackedIn in charOne as the return value of charTwo.attack()
			// return value is the attack points
			charTwo->attack();
			// Check to see if attacker is Medusa and rolls a 12
			if(charTwo->getName() == "Medusa" && charTwo->getAttackPoints() == 12)
			{
				cout << "Character 2 is Medusa and has rolled a 12. Character 1 is turned into stone with the"
						" Medusa glare. Character 2 wins." << endl;

				// charOne is Harry Potter and he is dying for the first time
				if(charOne->getName() == "Harry Potter" && charOne->getLives() == 0)
				{
					// If it is Harry Potter and it is his first time dying,
					// game is not over.
					cout << "But, Character 1 is Harry Potter and it is his first life." << endl;
					cout << "He uses Hogwarts to increase his strength to 20." << endl;

					charOne->setStrengthPoints(20);
					charOne->increaseLives();
					cout << "Harry Potter's current strength points are " << charOne->getStrengthPoints() << endl;
				}
				else // No one else can escape Medusa's glare so for everyone else game over
				{
					return 1;
				}
			}
			charOne->setDamageAttackedIn(charTwo->getAttackPoints());

			// Using the damageAttackedIn above charOne uses defense function to set
			// the strength points to adjusted value
			charOne->defense(charOne->getDamageAttackedIn());
		}
		else // don't attack
		{
			cout << "Character 1 is a vampire and charm was used so no attack by Character 2 or defense"
					" by character 1." << endl;
		}
	}
	else // charOne not vampire attack all the time
	{
		// Char 2 attacks char 1
		// Set the damageAttackedIn in charOne as the return value of charTwo.attack()
		// return value is the attack points
		charTwo->attack();
		// Check to see if attacker is Medusa and rolls a 12
		if(charTwo->getName() == "Medusa" && charTwo->getAttackPoints() == 12)
		{
			cout << "Character 2 is Medusa and has rolled a 12. Character 1 is turned into stone with the"
					" Medusa glare. Character 2 wins." << endl;

			// charOne is Harry Potter and he is dying for the first time
			if(charOne->getName() == "Harry Potter" && charOne->getLives() == 0)
			{
				// If it is Harry Potter and it is his first time dying,
				// game is not over.
				cout << "But, Character 1 is Harry Potter and it is his first life." << endl;
				cout << "He uses Hogwarts to increase his strength to 20." << endl;

				charOne->setStrengthPoints(20);
				charOne->increaseLives();
				cout << "Harry Potter's current strength points are " << charOne->getStrengthPoints() << endl;
			}
			else // No one else can escape Medusa's glare so for everyone else game over
			{
				return 1;
			}
		}
		charOne->setDamageAttackedIn(charTwo->getAttackPoints());

		// Using the damageAttackedIn above charOne uses defense function to set
		// the strength points to adjusted value
		charOne->defense(charOne->getDamageAttackedIn());
	}

	cout << "____________________________________" << endl;
	cout << "\n\n" << endl;

	// Round code finished

	// While both strength points are > 0 continue game
	while(charOne->getStrengthPoints() > 0 && charTwo->getStrengthPoints() > 0)
	{
		numRound++;

		// Round code start

		cout << "\n\n\n" << endl;
		cout << "Round # " << numRound << endl;

		cout << "Character 1 attack" << endl;
		cout << "Character 1 type " << charOne->getName();
		cout << "\n" << endl;

		// Char 1 attacks Char 2 and damageAttackedIn is set in charTwo
		if(charTwo->getName() == "Vampire") // charTwo Vampire
		{
			// If Char2 is vampire attack only 50% of the time
			// This will only happen 50% of the time
			// This code produces either a 2 or a 1 randomly
			chance = rand() % 2 + 1;

			if(chance == 1) // attack on getting 1 as a the random number
			{
				charOne->attack();
				// Check to see if attacker is Medusa and rolls a 12
				if(charOne->getName() == "Medusa" && charOne->getAttackPoints() == 12)
				{
					cout << "Character 1 is Medusa and has rolled a 12. Character 2 is turned into stone with the"
							" Medusa glare. Character 1 wins." << endl;


					// charTwo is Harry Potter and he is dying for the first time
					if(charTwo->getName() == "Harry Potter" && charTwo->getLives() == 0)
					{
						// If it is Harry Potter and it is his first time dying,
						// game is not over.
						cout << "But, Character 2 is Harry Potter and it is his first life." << endl;
						cout << "He uses Hogwarts to increase his strength to 20." << endl;

						charTwo->setStrengthPoints(20);
						charTwo->increaseLives();
						cout << "Harry Potter's current strength points are " << charTwo->getStrengthPoints() << endl;
					}
					else // No one else can escape Medusa's glare so for everyone else game over
					{
						return 1;
					}


				}
				charTwo->setDamageAttackedIn(charOne->getAttackPoints());

				// Using that damageAttackedIn from above
				// charTwo uses the defense function to set the strength points within charTwo
				charTwo->defense(charTwo->getDamageAttackedIn());

			}
			else // don't attack on 2
			{
				cout << "Character 2 is a vampire and charm was used so no attack by Character 1 or defense"
						" by character 2." << endl;
			}
		}
		else // charTwo not vampire
		{
			// If char2 is not a vampire attack all the time
			charOne->attack();
			// Check to see if attacker is Medusa and rolls a 12
			if(charOne->getName() == "Medusa" && charOne->getAttackPoints() == 12)
			{
				cout << "Character 1 is Medusa and has rolled a 12. Character 2 is turned into stone with the"
						" Medusa glare. Character 1 wins." << endl;

				// charTwo is Harry Potter and he is dying for the first time
				if(charTwo->getName() == "Harry Potter" && charTwo->getLives() == 0)
				{
					// If it is Harry Potter and it is his first time dying,
					// game is not over.
					cout << "But, Character 2 is Harry Potter and it is his first life." << endl;
					cout << "He uses Hogwarts to increase his strength to 20." << endl;

					charTwo->setStrengthPoints(20);
					charTwo->increaseLives();
					cout << "Harry Potter's current strength points are " << charTwo->getStrengthPoints() << endl;
				}
				else // No one else can escape Medusa's glare so for everyone else game over
				{
					return 1;
				}

			}
			charTwo->setDamageAttackedIn(charOne->getAttackPoints());

			// Using that damageAttackedIn from above
			// charTwo uses the defense function to set the strength points within charTwo
			charTwo->defense(charTwo->getDamageAttackedIn());
		}


		cout << "Character 2 attack" << endl;
		cout << "Character 2 type " << charTwo->getName();
		cout << "\n" << endl;

		// If charOne is a vampire
		// charTwo will only attack charOne 50% of the time
		if(charOne->getName() == "Vampire")
		{
			chance = rand() % 2 + 1;

			if(chance == 1) // attack
			{
				// Char 2 attacks char 1
				// Set the damageAttackedIn in charOne as the return value of charTwo.attack()
				// return value is the attack points
				charTwo->attack();
				// Check to see if attacker is Medusa and rolls a 12
				if(charTwo->getName() == "Medusa" && charTwo->getAttackPoints() == 12)
				{
					cout << "Character 2 is Medusa and has rolled a 12. Character 1 is turned into stone with the"
							" Medusa glare. Character 2 wins." << endl;

					// charOne is Harry Potter and he is dying for the first time
					if(charOne->getName() == "Harry Potter" && charOne->getLives() == 0)
					{
						// If it is Harry Potter and it is his first time dying,
						// game is not over.
						cout << "But, Character 1 is Harry Potter and it is his first life." << endl;
						cout << "He uses Hogwarts to increase his strength to 20." << endl;

						charOne->setStrengthPoints(20);
						charOne->increaseLives();
						cout << "Harry Potter's current strength points are " << charOne->getStrengthPoints() << endl;
					}
					else // No one else can escape Medusa's glare so for everyone else game over
					{
						return 1;
					}
				}
				charOne->setDamageAttackedIn(charTwo->getAttackPoints());

				// Using the damageAttackedIn above charOne uses defense function to set
				// the strength points to adjusted value
				charOne->defense(charOne->getDamageAttackedIn());
			}
			else // don't attack
			{
				cout << "Character 1 is a vampire and charm was used so no attack by Character 2 or defense"
						" by character 1." << endl;
			}
		}
		else // charOne not vampire attack all the time
		{
			// Char 2 attacks char 1
			// Set the damageAttackedIn in charOne as the return value of charTwo.attack()
			// return value is the attack points
			charTwo->attack();
			// Check to see if attacker is Medusa and rolls a 12
			if(charTwo->getName() == "Medusa" && charTwo->getAttackPoints() == 12)
			{
				cout << "Character 2 is Medusa and has rolled a 12. Character 1 is turned into stone with the"
						" Medusa glare. Character 2 wins." << endl;

				// charOne is Harry Potter and he is dying for the first time
				if(charOne->getName() == "Harry Potter" && charOne->getLives() == 0)
				{
					// If it is Harry Potter and it is his first time dying,
					// game is not over.
					cout << "But, Character 1 is Harry Potter and it is his first life." << endl;
					cout << "He uses Hogwarts to increase his strength to 20." << endl;

					charOne->setStrengthPoints(20);
					charOne->increaseLives();
					cout << "Harry Potter's current strength points are " << charOne->getStrengthPoints() << endl;
				}
				else // No one else can escape Medusa's glare so for everyone else game over
				{
					return 1;
				}
			}
			charOne->setDamageAttackedIn(charTwo->getAttackPoints());

			// Using the damageAttackedIn above charOne uses defense function to set
			// the strength points to adjusted value
			charOne->defense(charOne->getDamageAttackedIn());
		}

		cout << "____________________________________" << endl;
		cout << "\n\n" << endl;

		// Round code finished

	}

	return 0;
}


