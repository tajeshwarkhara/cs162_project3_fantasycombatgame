/*********************************************************************
** Program name: Character.h
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Abstract base class spec file.
*********************************************************************/

#ifndef PROJECT3_FANTASYCOMBATGAME_CHARACTER_H
#define PROJECT3_FANTASYCOMBATGAME_CHARACTER_H

#include <cstdlib> // for srand
#include <ctime>	// for time
#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::string;

class Character
{
protected:
	int armor = 0;
	int strengthPoints = 0;
	int lives = 0;
public:
	int getLives() const;

	void setLives(int lives);

public:
	void setStrengthPoints(int strengthPoints);

protected:
	string name = "Generic Character";
	int attackPoints = 0;
	int defensePoints = 0;
	int actualDamageInflicted = 0;
	int damageAttackedIn = 0;
public:
	int getDamageAttackedIn() const;

	void setDamageAttackedIn(int damageAttackedIn);

public:
	int getArmor() const;

	int getStrengthPoints() const;

	const string &getName() const;

	int getAttackPoints() const;

	int getDefensePoints() const;



public:
	// Constructor
	Character();

	// Optional constructor
	Character(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
			int actualDamageInflicted, int damageAttackedIn, int lives);

	// Destructor
	virtual ~Character();

	// Attack
	virtual int attack() = 0;

	// Defense - returns defenders updated strength points
	virtual int defense(int damageAttackedIn) = 0;

	// Print end formatting
	void printEndFormat();

	// Defense common code
	int defenseCommonCode();

	// Increase lives
	void increaseLives();

};


#endif //PROJECT3_FANTASYCOMBATGAME_CHARACTER_H
