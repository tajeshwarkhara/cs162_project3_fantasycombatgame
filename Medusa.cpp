/*********************************************************************
** Program name: Medusa.cpp
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Medusa class implementation file.
 * Child of Character.
*********************************************************************/

#include "Medusa.h"

// Default constructor
Medusa::Medusa()
		:Character(3,8,"Medusa", 0, 0, 0, 0, 0)
{
	// For seeding dies
	srand(time(0));
}

// Optional constructor
Medusa::Medusa(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
		int actualDamageInflictedIn, int damageAttackedIn, int livesIn)
		:Character(armorIn, strengthPointsIn, nameIn, attackPointsIn, defensePointsIn, actualDamageInflictedIn,
				damageAttackedIn, livesIn)
{
	// For seeding dies
	srand(time(0));
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void Medusa::attack()
** // Attack
// Rolling 2 - 6 sided dies
******************************************************************************/

int Medusa::attack()
{
	int rollOne = 0;
	int rollTwo = 0;
	rollOne = rand() % 6 + 1;
	rollTwo = rand() % 6 + 1;
	attackPoints = rollOne + rollTwo;

	cout << "Attack by " << name << ": " << endl;
	cout << "Roll one " << rollOne << endl;
	cout << "Roll two " << rollTwo << endl;
	cout << "Total attack points " << attackPoints << endl;

	printEndFormat();

	return attackPoints;
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void Medusa::defense()
** Defense
** 1 Six Sided dice
******************************************************************************/
int Medusa::defense(int damageAttackedIn)
{
	int rollOne = 0;
	rollOne = rand() % 6 + 1;

	defensePoints = rollOne;

	cout << "Defense by " << name << endl;
	cout << "Roll one " << rollOne << endl;

	strengthPoints = defenseCommonCode();

	return strengthPoints;
}

// Destructor
Medusa::~Medusa()
{

}
