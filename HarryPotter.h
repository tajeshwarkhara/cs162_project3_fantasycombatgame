/*********************************************************************
** Program name: HarryPotter.h
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: HarryPotter class specification file.
 * Child of Character.
*********************************************************************/

#ifndef PROJECT3_FANTASYCOMBATGAME_HARRYPOTTER_H
#define PROJECT3_FANTASYCOMBATGAME_HARRYPOTTER_H


#include "Character.h"

class HarryPotter : public Character
{

public:
	// Default constructor
	HarryPotter();

	// Optional constructor
	HarryPotter(int lives, int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
			int actualDamageInflictedIn, int damageAttackedIn, int livesIn);

	// Destructor
	virtual ~HarryPotter();

	// Attack
	virtual int attack();

	// Defense
	virtual int defense(int actualDamageAttackedIn);

	// Hogwarts
	void hogwarts();


};


#endif //PROJECT3_FANTASYCOMBATGAME_HARRYPOTTER_H
