/*********************************************************************
** Program name: Barbarian.h
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Barbarian class specification file.
 * Child of Character.
*********************************************************************/

#ifndef PROJECT3_FANTASYCOMBATGAME_BARBARIAN_H
#define PROJECT3_FANTASYCOMBATGAME_BARBARIAN_H


#include "Character.h"

class Barbarian : public Character
{

public:
	// Default constructor
	Barbarian();

	// Optional constructor
	Barbarian(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
			int actualDamageInflictedIn, int damageAttackedIn, int livesIn);

	// Destructor
	virtual ~Barbarian();

	// Attack
	virtual int attack();

	// Defense
	virtual int defense(int damageAttackedIn);
};


#endif //PROJECT3_FANTASYCOMBATGAME_BARBARIAN_H
