/*********************************************************************
** Program name: BlueMen.h
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: BlueMen class specification file.
 * Child of Character.
*********************************************************************/

#ifndef PROJECT3_FANTASYCOMBATGAME_BLUEMEN_H
#define PROJECT3_FANTASYCOMBATGAME_BLUEMEN_H


#include "Character.h"

class BlueMen : public Character
{

public:
	// Default constructor
	BlueMen();

	// Optional constructor
	BlueMen(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
			int actualDamageInflictedIn, int damageAttackedIn, int livesIn);

	// Destructor
	virtual ~BlueMen();

	// Attack
	virtual int attack();

	// Defense
	virtual int defense(int damageAttackedIn);

	// Charm
	void mob();
};


#endif //PROJECT3_FANTASYCOMBATGAME_BLUEMEN_H
