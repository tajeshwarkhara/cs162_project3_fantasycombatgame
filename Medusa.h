/*********************************************************************
** Program name: Medusa.h
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Medusa class specification file.
 * Child of Character.
*********************************************************************/

#ifndef PROJECT3_FANTASYCOMBATGAME_MEDUSA_H
#define PROJECT3_FANTASYCOMBATGAME_MEDUSA_H


#include "Character.h"

class Medusa : public Character
{

public:
	// Default constructor
	Medusa();

	// Optional constructor
	Medusa(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
			int actualDamageInflictedIn, int damageAttackedIn, int livesIn);

	// Destructor
	virtual ~Medusa();

	// Attack
	virtual int attack();

	// Defense
	virtual int defense(int damageAttackedIn);
};


#endif //PROJECT3_FANTASYCOMBATGAME_MEDUSA_H
