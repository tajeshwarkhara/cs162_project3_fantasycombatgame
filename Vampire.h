/*********************************************************************
** Program name: Vampire.h
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Vampire class specification file.
 * Child of Character.
*********************************************************************/

#ifndef PROJECT3_FANTASYCOMBATGAME_VAMPIRE_H
#define PROJECT3_FANTASYCOMBATGAME_VAMPIRE_H


#include "Character.h"

class Vampire : public Character
{

public:
	// Default constructor
	Vampire();

	// Optional constructor
	Vampire(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
			int actualDamageInflictedIn, int damageAttackedIn, int livesIn);

	// Destructor
	virtual ~Vampire();

	// Attack
	virtual int attack();

	// Defense
	// Returns strength points
	virtual int defense(int damageAttackedIn);

	// Charm
	void charm();
};


#endif //PROJECT3_FANTASYCOMBATGAME_VAMPIRE_H
