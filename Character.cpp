/*********************************************************************
** Program name: Character.cpp
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Abstract base class implementation file.
*********************************************************************/

#include "Character.h"


// Default constructor
Character::Character()
{
	this->armor = 0;
	this->strengthPoints = 0;
	this->name = "Generic character";
	this->attackPoints = 0;
	this->defensePoints = 0;
	this->actualDamageInflicted = 0;
	this->damageAttackedIn = 0;
	this->lives = 0;

}


// Optional constructor
Character::Character(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
		int actualDamageInflicted, int damageAttackedIn, int lives)
{
	this->name = nameIn;
	this->armor = armorIn;
	this->strengthPoints = strengthPointsIn;
	this->attackPoints = attackPointsIn;
	this->defensePoints = defensePointsIn;
	this->actualDamageInflicted = actualDamageInflicted;
	this->damageAttackedIn = damageAttackedIn;
	this->lives = lives;
}

int Character::getArmor() const {
	return armor;
}

int Character::getStrengthPoints() const {
	return strengthPoints;
}

const string &Character::getName() const {
	return name;
}

int Character::getAttackPoints() const {
	return attackPoints;
}

int Character::getDefensePoints() const {
	return defensePoints;
}

void Character::printEndFormat()
{
	cout << "\n\n\n" << endl;
}

// Getter
int Character::getDamageAttackedIn() const {
	return damageAttackedIn;
}

// Setter
void Character::setDamageAttackedIn(int damageAttackedIn) {
	Character::damageAttackedIn = damageAttackedIn;
}


//Setter
void Character::setStrengthPoints(int strengthPoints) {
	Character::strengthPoints = strengthPoints;
}


// Common code from all the defense functions
int Character::defenseCommonCode()
{
	cout << "Defense points " << defensePoints << endl;

	actualDamageInflicted = damageAttackedIn - defensePoints - armor;

	cout << "Damage attacked in " << damageAttackedIn << endl;
	cout << "Armor " << armor << endl;
	cout << "Actual Damage Inflicted " << actualDamageInflicted << endl;
	cout << "Strength points at start " << strengthPoints << endl;

	// Apply the damage to the defender's strength points
	strengthPoints = strengthPoints - actualDamageInflicted;

	if(strengthPoints <= 0)
	{
		cout << "The character " << name << " is dead. " << endl;
		cout << "Strength points currently " << strengthPoints << endl;
	}
	else
	{
		cout << "Strength points currently " << strengthPoints << endl;
	}

	printEndFormat();

	return strengthPoints;
}

Character::~Character()
{

}


// Getters and setters for lives
int Character::getLives() const {
	return lives;
}

void Character::setLives(int lives) {
	Character::lives = lives;
}

// Increase lives
void Character::increaseLives()
{
	lives += 1;
}
