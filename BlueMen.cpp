/*********************************************************************
** Program name: BlueMen.cpp
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: BlueMen class implementation file.
 * Child of Character.
*********************************************************************/

#include "BlueMen.h"

// Default constructor
BlueMen::BlueMen()
		:Character(3,12,"Blue Men", 0, 0, 0, 0, 0)
{
	// For seeding dies
	srand(time(0));
}

// Optional constructor
BlueMen::BlueMen(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
		int actualDamageInflictedIn, int damageAttackedIn, int livesIn)
		:Character(armorIn, strengthPointsIn, nameIn, attackPointsIn, defensePointsIn, actualDamageInflictedIn,
				damageAttackedIn, livesIn)
{
	// For seeding dies
	srand(time(0));
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void BlueMen::attack()
** // Attack
// Rolling 2 - 10 sided dies
******************************************************************************/

int BlueMen::attack()
{
	int rollOne = 0;
	int rollTwo = 0;
	rollOne = rand() % 10 + 1;
	rollTwo = rand() % 10 + 1;
	attackPoints = rollOne + rollTwo;

	cout << "Attack by " << name << ": " << endl;
	cout << "Roll one " << rollOne << endl;
	cout << "Roll two " << rollTwo << endl;
	cout << "Total attack points " << attackPoints << endl;

	printEndFormat();

	return attackPoints;
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void BlueMen::defense()
** Defense
// 3 Six Sided dice
// With Mob
// Mob: Blue Men are actually a swarm of small individuals. For every 4
** points of damage, they lose one defense die. For example, if they have a
 * strength of 8, they would have 2d6 for defense.
******************************************************************************/
int BlueMen::defense(int damageAttackedIn)
{
	int rollOne = 0;
	int rollTwo = 0;
	int rollThree = 0;

	if(strengthPoints > 0 && strengthPoints <= 4)
	{
		rollOne = 0;
		rollTwo = 0;
		rollThree = 0;

		cout << "Defense by " << name << endl;
		mob();
		cout << "Strength points are less than or equal to 4. Only one die rolled." << endl;
		rollOne = rand() % 6 + 1;

		defensePoints = rollOne;


		cout << "Roll one " << rollOne << endl;
	}
	else if(strengthPoints > 4 && strengthPoints <= 8)
	{
		rollOne = 0;
		rollTwo = 0;
		rollThree = 0;

		cout << "Defense by " << name << endl;
		mob();
		cout << "Strength points are less than or equal to 8. Only two die rolled." << endl;
		rollOne = rand() % 6 + 1;
		rollTwo = rand() % 6 + 1;

		defensePoints = rollOne + rollTwo + rollThree;


		cout << "Roll one " << rollOne << endl;
		cout << "Roll two " << rollTwo << endl;
	}
	else
	{
		rollOne = 0;
		rollTwo = 0;
		rollThree = 0;

		cout << "Defense by " << name << endl;
		cout << "Strength points are greater than 8. All three die rolled." << endl;

		rollOne = rand() % 6 + 1;
		rollTwo = rand() % 6 + 1;
		rollThree = rand() % 6 + 1;

		defensePoints = rollOne + rollTwo + rollThree;


		cout << "Roll one " << rollOne << endl;
		cout << "Roll two " << rollTwo << endl;
		cout << "Roll three " << rollThree << endl;
	}

	strengthPoints = defenseCommonCode();

	return strengthPoints;
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Function called
** void BlueMen::charm()
// With Mob
// Mob: Blue Men are actually a swarm of small individuals. For every 4
** points of damage, they lose one defense die. For example, if they have a
 * strength of 8, they would have 2d6 for defense.
 * [To Be Implemented]
******************************************************************************/
void BlueMen::mob()
{
	cout << "BlueMen mob applied. " << endl;
	cout << "For every 4 points of damage, they lose one defense die." << endl;
}

BlueMen::~BlueMen()
{

}
