/*********************************************************************
** Program name: Barbarian.cpp
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Barbarian class implementation file.
 * Child of Character.
*********************************************************************/

#include "Barbarian.h"


// Default constructor
Barbarian::Barbarian()
		:Character(0,12,"Barbarian", 0, 0, 0, 0, 0)
{
	// For seeding dies
	srand(time(0));
}

// Optional constructor
Barbarian::Barbarian(int armorIn, int strengthPointsIn, string nameIn, int attackPointsIn, int defensePointsIn,
		int actualDamageInflictedIn, int damageAttackedIn, int livesIn)
		:Character(armorIn, strengthPointsIn, nameIn, attackPointsIn, defensePointsIn, actualDamageInflictedIn,
				damageAttackedIn, livesIn)
{
	// For seeding dies
	srand(time(0));
}

// Attack
// Rolling 2 6 sided die
int Barbarian::attack()
{
	int rollOne = 0;
	int rollTwo = 0;
	rollOne = rand() % 6 + 1;
	rollTwo = rand() % 6 + 1;
	attackPoints = rollOne + rollTwo;

	cout << "Attack by " << name << ": " << endl;
	cout << "Roll one " << rollOne << endl;
	cout << "Roll two " << rollTwo << endl;
	cout << "Total attack points " << attackPoints << endl;

	printEndFormat();

	return attackPoints;
}

// Defense
int Barbarian::defense(int damageAttackedIn)
{
	int rollOne = 0;
	int rollTwo = 0;
	rollOne = rand() % 6 + 1;
	rollTwo = rand() % 6 + 1;
	defensePoints = rollOne + rollTwo;
	cout << "Defense by " << name << endl;
	cout << "Roll one " << rollOne << endl;
	cout << "Roll two " << rollTwo << endl;

	strengthPoints = defenseCommonCode();

	return strengthPoints;
}

// Destructor
Barbarian::~Barbarian()
{

}


