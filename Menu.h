/*********************************************************************
** Program name: Menu.h
** Author: Tajeshwar Singh Khara
** Date: 23-10-2018
** Description: Menu Class specification file
*********************************************************************/

#ifndef PROJECT3_FANTASYCOMBATGAME_MENU_H
#define PROJECT3_FANTASYCOMBATGAME_MENU_H

#include <iostream>
using std::cout;
using std::endl;
using std::cin;
#include <iomanip>
#include <climits>
using std::setw;

#include <string>
using std::string;

#include "Character.h"
#include "Vampire.h"
#include "Barbarian.h"
#include "BlueMen.h"
#include "Medusa.h"
#include "HarryPotter.h"


class Menu
{

private:
	Character* charPtr1 = nullptr;
	Character* charPtr2 = nullptr;


public:
	int getFirstChoice() const;
	int getSecondChoice() const;

private:
	int firstChoice = 0;
	int secondChoice = 0;
public:

	template <typename T>
	void getChoice(T& choiceIn, string input);

	void displayCharacters();

	template <typename T>
	void playAgainGetChoice(T& choice, string input);

	int playAgain();


	// play from main (refactor)
	int play(Character* charOne, Character* charTwo);

	// Initial setup from main (refactor)
	void initialSetup();

	// Constructor
	Menu();


};


#endif //PROJECT3_FANTASYCOMBATGAME_MENU_H
